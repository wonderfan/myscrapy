#! /bin/bash
list=$(pidof -o %PPID python)

for p in $list
do
  echo "Killing $p..."
  kill $p
done

echo "-----Staring UI and Worker-----"
cd CrawlUI
pwd
nohup python app.py >ui.log 2>&1 &
cd ../linksCrawl
pwd
nohup python worker.py >task.log 2>&1 &
echo "-----Start UI and Worker successfully-----"