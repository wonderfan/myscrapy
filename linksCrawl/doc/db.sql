CREATE TABLE links ( link character varying(255),source character varying(255));
ALTER TABLE ONLY links ADD CONSTRAINT links_pkey PRIMARY KEY (link);

ALTER TABLE links ADD COLUMN date DATE;
ALTER TABLE links ALTER COLUMN date SET DEFAULT CURRENT_DATE;
alter table links add column code varchar(25);
alter table links add column status varchar(25);