# -*- coding: utf-8 -*-
import scrapy
from tld import get_tld
from linksCrawl.items import LinkscrawlItem
from linksCrawl import settings
from urlparse import urlparse

class LinkspiderSpider(scrapy.Spider):
    name = "linkSpider"
    start_urls = settings.START_URLS

    def parse(self, response):
        domain = get_tld(response.url)
        url_result = urlparse(response.url)
        top_domain = url_result.scheme + '://'+url_result.netloc
        for sel in response.xpath('//a/@href'):
            item = LinkscrawlItem()
            link = sel.extract()
            if link.find("http://") == 0 or link.find("https://") == 0 or link.find("www.") == 0:
                try:
                    target_domain = get_tld(link)
                    if domain != target_domain:
                        item['link'] = target_domain
                        item['source'] = top_domain
                        item['code'] = str(response.status)
                        yield item
                    else:
                        yield scrapy.Request(link,callback=self.parse)
                except Exception as e:
                    print e
                    print "The url can't get the domain. Ignored..." + link
            else:
                if link.startswith('mailto'):
                    continue
                if link.startswith('./'):
                    link = link[1:]
                if link.startswith('../'):
                    link = link[2:]
                if not link.startswith('/'):
                    link = '/'+link
                yield scrapy.Request(top_domain + link,callback=self.parse)
