# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
from linksCrawl import settings
import psycopg2
import sqlalchemy.pool as pool
from scrapy.exceptions import DropItem
from dynadotpy.client import Dynadot

SEARCH_RESPONSES = {
    "yes": "The domain is available.",
    "no": "The domain is not available.",
    "offline": "The central registry for this domain is currently offline.",
    "system_busy": "All connections are busy.",
    "over_quota": "Over quota.",
    "error": "There was a syntax or registry error processing this domain."
}

def get_domain_status(domain):
    dyn = Dynadot(api_key="7m9P8D6H8LC72726J6I9P9M8IE6k6fN8i6m8O7U7pC7u")
    domains=[domain]
    result = dyn.search(domains=domains)
    if result is None:
        return SEARCH_RESPONSES['no']
    else:
        if isinstance(result,list):
            if len(result) > 0:
                rs = result[0]
                return SEARCH_RESPONSES[rs['result']]
        else:
            return SEARCH_RESPONSES['no']

def getconn():
    conn = psycopg2.connect(dbname=settings.DB_NAME, user=settings.DB_USER,
                                     password=settings.DB_PASS, host=settings.DB_HOST, port=settings.DB_PORT)
    return conn

mypool = pool.QueuePool(getconn, max_overflow=10, pool_size=5)

class LinkscrawlPipeline(object):
    def __init__(self):
        pass

    def process_item(self, item, spider):
        conn = mypool.connect()
        cur = conn.cursor()
        flag = False
        try:
 #           cur.execute("select link,source from domains where link = %s and source = %s",(item['link'], item['source']))
            cur.execute("select link,source from domains where link = %s ",(item['link'],))
            row = cur.fetchone()
            if row is not None:
                flag = True
                cur.close()
                conn.close()
        except Exception as e:
            print "can not perform the query." + str(e)
            
        if flag :
            raise DropItem("It already exists in the database") 
        else:
            try:
                #status = get_domain_status(item['link'])
                status = 'unchecked'
                cur.execute("INSERT INTO domains (link,source,status) VALUES (%s,%s,%s)", (item['link'], item['source'],status,))
                conn.commit()
            except Exception as e:
                print "ignoring duplicated url. " + str(e)
            cur.close()
            conn.close()
            return item
