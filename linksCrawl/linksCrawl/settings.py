# -*- coding: utf-8 -*-

# Scrapy settings for linksCrawl project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'linksCrawl'

SPIDER_MODULES = ['linksCrawl.spiders']
NEWSPIDER_MODULE = 'linksCrawl.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'linksCrawl (+http://www.yourdomain.com)'
ITEM_PIPELINES = {'linksCrawl.pipelines.LinkscrawlPipeline': 1}

#uncomment following config to enable proxy
# DOWNLOADER_MIDDLEWARES = {
#      'scrapy.contrib.downloadermiddleware.retry.RetryMiddleware': 90,
#      'linksCrawl.RandomProxy.RandomProxy': 100,
#      'scrapy.contrib.downloadermiddleware.httpproxy.HttpProxyMiddleware': 110,
# }

# Retry many times since proxies often fail
RETRY_TIMES = 10
# Retry on most error codes since proxies fail for different reasons
RETRY_HTTP_CODES = [500, 503, 504, 400, 403, 404, 408]


DB_HOST = 'localhost'
DB_USER = 'postgres'
DB_PASS = 'postgres'
DB_NAME = 'crawl'
DB_PORT = '5432'

START_URLS = ()
LOG_LEVEL = 'CRITICAL'