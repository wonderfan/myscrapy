#!/usr/bin/env python
import pika
import subprocess
import time

def callback(ch, method, properties, body):
    print "The message is %s " % body
    urls = body.split(',')
    if urls is not None:
        for url in urls:
            if url is not None and url != '':
                url = url.strip()
                print "the url is %s " % url
                subprocess.call('scrapy parse --spider=linkSpider --pipelines -d 5 '+ url, shell=True)
                #time.sleep(3)
    
    
def main():
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    #channel.queue_declare(queue='task',durable=True)
    channel.queue_declare(queue='task')
    channel.basic_consume(callback,queue='task',no_ack=True)
    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()
    
    connection.close()
    
if __name__ == '__main__':
    main()