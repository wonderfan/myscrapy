from flask import Flask,render_template,url_for,redirect,request,session,flash,Response
from flask.ext import admin
import subprocess
import settings
import psycopg2
from urlparse import urlparse
import os
import pika
from dynadotpy.client import Dynadot

SEARCH_RESPONSES = {
    "yes": "The domain is available.",
    "no": "The domain is not available.",
    "offline": "The central registry for this domain is currently offline.",
    "system_busy": "All connections are busy.",
    "over_quota": "Over quota.",
    "error": "There was a syntax or registry error processing this domain."
}

def get_domain_status(domain):
    dyn = Dynadot(api_key="7m9P8D6H8LC72726J6I9P9M8IE6k6fN8i6m8O7U7pC7u")
    domains=[domain]
    result = dyn.search(domains=domains)
    if result is None:
        return SEARCH_RESPONSES['no']
    else:
        if isinstance(result,list):
            if len(result) > 0:
                rs = result[0]
                return SEARCH_RESPONSES[rs['result']]
        else:
            return SEARCH_RESPONSES['no']

def send_message(body):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
    channel = connection.channel()
    channel.queue_declare(queue='task')
    channel.basic_publish(exchange='',routing_key='task',body=body)
    print "sent the message: %s" % body
    connection.close()

def get_domains():
    try:
        conn = psycopg2.connect(dbname=settings.DB_NAME,user=settings.DB_USER,password=settings.DB_PASS,host=settings.DB_HOST,port=settings.DB_PORT)
    except:
        flash('unable to connect to database','error')
    cur = conn.cursor()
    sql = 'select distinct source from domains order by source asc'
    cur.execute(sql)
    rows = cur.fetchall()
    conn.close()
    data = []
    for row in rows:
        url = row[0]
        rs = urlparse(url.strip())
        data.append(rs.netloc)
    return data

def get_data(domain):
    try:
        conn=psycopg2.connect(dbname=settings.DB_NAME,user=settings.DB_USER,password=settings.DB_PASS,host=settings.DB_HOST,port=settings.DB_PORT)
    except:
        flash('unable to connect to database','error')
    cur = conn.cursor()
    sql = "select * from domains where source like '%" + domain.strip() + "'"
    cur.execute(sql)
    data = cur.fetchall()
    conn.close()
    return data
    
def get_data_by_date(start,end):
    try:
        conn=psycopg2.connect(dbname=settings.DB_NAME,user=settings.DB_USER,password=settings.DB_PASS,host=settings.DB_HOST,port=settings.DB_PORT)
    except:
        flash('unable to connect to database','error')
    cur = conn.cursor()
    sql = "select * from domains "
    if start is not None:
        sql = sql + " where date >= '" + start.strip() + "'"
        if end is not None and len(end.strip())>0:
            sql = sql + " and date <= '" + end.strip() + "'"
    print sql
    cur.execute(sql)
    data = cur.fetchall()
    conn.close()
    return data    

def delete(domain):
    try:
        conn = psycopg2.connect(dbname=settings.DB_NAME,user=settings.DB_USER,password=settings.DB_PASS,host=settings.DB_HOST,port=settings.DB_PORT)
    except:
        flash('unable to connect to database','error')
    cur = conn.cursor()
    sql = "delete from domains where source like '%" + domain.strip() + "'"
    cur.execute(sql)
    conn.commit()
    conn.close()
app = Flask(__name__)
app.config['SECRET_KEY'] = 'scrapy123456790'

@app.route('/')
def index():
    return redirect('/admin/')

class HomeView(admin.AdminIndexView):
    @admin.expose('/',methods=['GET','POST'])
    def index(self):
        if request.method == 'POST':
            url = request.form['url']
            if url is None or url == '':
                flash("The URL cant be empty!",'error')
            else:
                data = ",".join(x.strip() for x in url.split('\r\n'))
                os.system('cd ' + settings.CRAWL_PROJECT_PATH)
                #subprocess.call("cd " + settings.CRAWL_PROJECT_PATH + '\n scrapy parse --spider=linkSpider --pipelines -d 6 '+url, shell=True)
                send_message(data)
                flash("Crawl the domain successuflly",'success')
        else:
            url = ""
        return self.render('home.html',url=url)

class ProxyView(admin.BaseView):
    @admin.expose('/',methods=['GET', 'POST'])
    def index(self):
        file_name = settings.CRAWL_PROJECT_PATH + '/linksCrawl/list.txt'
        if request.method=="POST":
            content = request.form['proxy']
            with open(file_name,'w') as fw:
                fw.write(content)
            flash("The proxy is saved successuflly","success")
        else:
            if os.path.exists(file_name):
                with open(file_name) as fr:
                    content = fr.read()
            else:
                content = ''

        return self.render('proxy.html',data=content)
        
class LinksView(admin.BaseView):
    @admin.expose('/',methods=['GET','POST'])
    def index(self):
        domains = get_domains()
        return self.render('links.html',domains=domains)
        
    @admin.expose('/download/<domain>')
    def download(self,domain):
        if domain is None:
            flash("The domain is blank","error")
            domains = get_domains()
            return self.render('links.html',domains=domains)
        def generate(domain):
            end = len(domain) -4
            domain = domain[0:end]
            rows = get_data(domain)
            for row in rows:
                yield ','.join((row[0],row[1],row[2].strftime('%m/%d/%Y'),row[3])) + '\n'
        return Response(generate(domain), mimetype='text/csv')
        
    @admin.expose('/delete/<domain>')
    def delete(self,domain):
        if domain is None:
            flash("The domain is blank","error")
        else:
            flash("The domain is deleted successuflly","success")
            delete(domain)
        domains = get_domains()
        return self.render('links.html',domains=domains)
        
class SearchView(admin.BaseView):
    @admin.expose('/',methods=['GET','POST'])
    def index(self):
        if request.method == 'POST':
            start = request.form['start']
            end = request.form['end']
            def generate(start,end):
                rows = get_data_by_date(start,end)
                for row in rows:
                    yield ','.join((row[0],row[1],row[2].strftime('%m/%d/%Y'),row[3])) + '\n'
            response = Response(generate(start,end), mimetype='text/csv',content_type='text/csv')
            response.headers['Content-Disposition'] = 'attachment; filename="domains.csv"'
            return response
        else:
            start = ''
            end = ''
        return self.render('search.html',start=start,end=end)
        
class CheckView(admin.BaseView):
    @admin.expose('/',methods=['GET','POST'])
    def index(self):
        status= {}
        if request.method == 'POST':
            domain = request.form['domain']
            if domain is None or domain == '':
                flash("The domain cant be empty!",'error')
            else:
                data = ",".join(x.strip() for x in domain.split('\r\n'))
                domains = data.split(',')
                if domains is not None:
                    for item in domains:
                        if item is not None and item != '':
                            item = item.strip()
                            status[item]=get_domain_status(item)
                flash("Check the domains successfully",'success')
        else:
            domain = ""
        return self.render('check.html',domain=domain,status=status)      

admin = admin.Admin(app, '',index_view=HomeView())
admin.add_view(LinksView(name="Links"))
admin.add_view(SearchView(name="Search"))
admin.add_view(CheckView(name="Check"))
admin.add_view(ProxyView(name="Proxy"))


if __name__ == '__main__':
    app.run(host='0.0.0.0',port=8080)
